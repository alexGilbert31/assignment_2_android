package com.goodies.alex.spotapp.fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import com.goodies.alex.spotapp.DataProvider;
import com.goodies.alex.spotapp.R;
import com.goodies.alex.spotapp.classHelper.TagsListAdapter;

import java.util.ArrayList;

import static android.R.attr.fragment;


public class TagsFragment extends Fragment {


    public ListView list;
    public ArrayList<DataProvider.Marker> listArr;

    public TagsListAdapter adapter;

    public View view;

    public TagsFragment() {
        // Required empty public constructor
        Log.d("Constructor", "Caller");


    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);





    }



    public void addTag (DataProvider.Marker marker) {
        listArr.add(marker);
        adapter.notifyDataSetChanged();
    }


    public void populateTagsList () {
        list = (ListView) view.findViewById(R.id.listView);
        listArr = new ArrayList<>();


        DataProvider.Marker markers[]  = DataProvider.getInstance().GetAllMarkers();


        for (DataProvider.Marker marker : markers) {
            listArr.add(marker);
        }



        adapter = new TagsListAdapter(getActivity().getBaseContext(),  listArr);

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Snackbar.make(view, "This is a tag!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }
        });



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("Constructor", "Caller");



        view = inflater.inflate(R.layout.fragment_tags, container, false);

        populateTagsList();




        // Inflate the layout for this fragment
        return view;
    }

}
