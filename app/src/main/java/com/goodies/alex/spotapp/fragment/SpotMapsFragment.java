package com.goodies.alex.spotapp.fragment;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.goodies.alex.spotapp.DataProvider;
import com.goodies.alex.spotapp.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class SpotMapsFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback {


    private static View rootView;

    private GoogleMap globalMap;


    private GoogleApiClient mGoogleApiClient;

    static public Location mLastLocation;
    private double mLatitude;
    private double mLongitude;


    public SpotMapsFragment() {
        // Required empty public constructor
        Log.d("Constructor", "Caller");

        //Fake creation ressolve that ! TODO:
        //49.283910, -123.125149 IP Robson Home
        DataProvider.getInstance().CreateCloseMarker(5, 49.283910, -123.125149 );

    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

        }

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fabAddTag);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTagOnMap(view);


            }
        });



    }


    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //View rootView = inflater.inflate(R.layout.fragment_layout, container, false);




        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_maps, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is  */
        }

        return rootView;


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                Toast toast = Toast.makeText(getContext(), "Didnt connect", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(getView(), "Problem with permision", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            mLatitude = mLastLocation.getLatitude();
            mLongitude = mLastLocation.getLongitude();
            Log.d("Lat", String.valueOf(mLatitude));
            Log.d("Lon", String.valueOf(mLongitude));
            Log.d("onConnected", "CALLER");



        }

        SupportMapFragment m = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.GoogleMapsFragment);
        m.getMapAsync(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void addTagOnMap (View view) {
        globalMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLatitude, mLongitude))
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromAsset("marker_blue.png"))
                .title("House"));

        DataProvider.getInstance().Create(mLatitude, mLongitude);

        //Update list with last marker TODO:
    /*   DataProvider.Marker marker =  DataProvider.getInstance().GetAllMarkers()[DataProvider.getInstance().GetAllMarkers().length-1];
       TagsFragment tag =  (TagsFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.tagsListFragment);
        TagsFragment tag2 =  (TagsFragment) getChildFragmentManager().findFragmentById(R.id.tagsListFragment);
        //
        tag.listArr.add(marker);

*/
        String lengthData = String.valueOf(DataProvider.getInstance().markers.size());


        Snackbar.make(view, lengthData, Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();


    }



    @Override
    public void onMapReady(GoogleMap map) {
        globalMap = map;
        Log.d("Caller ON", "Marche Location");




        //Creation of marker
        DataProvider.Marker[] markers =  DataProvider.getInstance().GetAllMarkers();


        //Populate list after ceation
      /*  TagsFragment tabsFragment =  (TagsFragment)getFragmentManager().findFragmentById(R.id.tagsListFragment);
        if (tabsFragment !=null) {
            tabsFragment = (TagsFragment)getFragmentManager().findFragmentById(R.id.tagsListFragment);
            tabsFragment.populateTagsList();
        } */
       // tabsFragment.populateTagsList();



        for (DataProvider.Marker markerDB: markers) {
            map.addMarker(new MarkerOptions()
                    .position(new LatLng(markerDB.getLatitude(), markerDB.getLongitude()))
                    .draggable(false)
                    .icon(BitmapDescriptorFactory.fromAsset(markerDB.getPathAssetPicture()))
                    .title(markerDB.getTitle()));
        }



      /*  map.addMarker(new MarkerOptions()
                .position(new LatLng(49.2803583, -123.1030802))
                .draggable(false)

                .icon(BitmapDescriptorFactory.fromAsset("marker_blue.png"))
                .title("The Message"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(49.280273, -123.101812))
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromAsset("marker_orange.png"))
                .title("Chinatown"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng(49.280073, -123.101412))
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromAsset("marker_blue.png"))
                .title("new1"));

        map.addMarker(new MarkerOptions()
                .position(new LatLng( 49.280413, -123.101212))
                .draggable(false)
                .icon(BitmapDescriptorFactory.fromAsset("marker_orange.png"))
                .title("new2"));
8*/
        GoogleMapOptions options = new GoogleMapOptions();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 14.0f));



        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
            Log.d("Marche Pas", "Marche pas Location");
        }
        map.setMyLocationEnabled(true);
        Log.d("Location ON", "Marche Location");
    }

}
