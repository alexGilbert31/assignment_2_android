package com.goodies.alex.spotapp.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;

import android.view.View;

import com.goodies.alex.spotapp.R;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Toolbar toolbar;

    private static final int WIKITUDE_PERMISSIONS_REQUEST_CAMERA = 0;
    private static final int WIKITUDE_PERMISSIONS_REQUEST_GPS = 0;
    private static final int WIKITUDE_PERMISSIONS_NETWORK_STATE = 0;
    private static final int WIKITUDE_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;
    private static final int WIKITUDE_PERMISSIONS_INTERNET = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button cloudBtn = (Button) findViewById(R.id.cloudBtn);
        Button closeBtn = (Button) findViewById(R.id.closeBtn);
        Button compassBtn = (Button) findViewById(R.id.compassBtn);

        cloudBtn.setOnClickListener(this);
        closeBtn.setOnClickListener(this);
        compassBtn.setOnClickListener(this);

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, WIKITUDE_PERMISSIONS_REQUEST_GPS);
        }

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, WIKITUDE_PERMISSIONS_NETWORK_STATE);
        }

        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, WIKITUDE_PERMISSIONS_REQUEST_CAMERA);
        }

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WIKITUDE_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        }
        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, WIKITUDE_PERMISSIONS_INTERNET);
        }


        String backCameraId = null;
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            for(String cameraId:manager.getCameraIdList()){
                CameraCharacteristics cameraCharacteristics = manager.getCameraCharacteristics(cameraId);
                Integer facing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                if(facing== CameraMetadata.LENS_FACING_BACK) {
                    backCameraId = cameraId;
                    break;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.d("CAMERA", "back camera exists ? "+(backCameraId!=null));
        Log.d("CAMERA", "back camera id  :"+backCameraId);

  }


    @Override
    public void onClick(View v) {
        // do something when the button is clicked
        // Yes we will handle click here but which button clicked??? We don't know

        // So we will make
        switch (v.getId() /*to get clicked view id**/) {
            case R.id.cloudBtn:

                 startActivity(new Intent(MainActivity.this, TabsActivity.class));

                break;
            case R.id.closeBtn:
                startActivity(new Intent(MainActivity.this, WikitudeCamera.class));
                break;

            case R.id.compassBtn:
                startActivity(new Intent(MainActivity.this, OrientationActivity.class));
                break;
            default:
                break;
        }
    }




}
