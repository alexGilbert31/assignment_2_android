package com.goodies.alex.spotapp;

import android.util.Log;

import com.goodies.alex.spotapp.classHelper.LocationProvider;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.wearable.Asset;


import java.util.ArrayList;
import java.util.Dictionary;

/**
 * Created by alex on 2016-12-02.
 */

public class DataProvider {


    public ArrayList<Marker> markers;

    private static DataProvider _instance;

    DataProvider() {
    }

    public static DataProvider getInstance() {
        if (_instance == null) {
            _instance = new DataProvider();
        }
        return _instance;
    }

    public void CreateCloseMarker(int numOfMarkers, double latitude, double longitude) {
        markers = new ArrayList<Marker>();

        for (int num = 0; num < numOfMarkers; num++) {
            Marker marker = new Marker();
            double randomLatitude = (latitude + Math.random() / 5 - 0.1);
            double randomLongitude = (longitude + Math.random() / 5 - 0.1);
            marker.Create(randomLatitude, randomLongitude);
            markers.add(marker);
        }

    }

    public void Create(double latitude, double longitude) {
        Marker marker = new Marker();
        marker.Create(latitude, longitude);
        markers.add(marker);
    }

    public Marker[] GetAllMarkers() {
        if (markers == null) {
            Log.d("No marker created", "");
            return null;
        }
        Marker[] markerArray = markers.toArray(new Marker[markers.size()]);


        Log.d("Marker created", "");
        return markerArray;
    }

    public String GetAllMarkersJSON() {
        if (markers == null) {
            Log.d("No marker created", "");
            return "empty";
        }
        String str = "[";

        for (int i = 0;i<markers.size();i++) {
            str += "{title:' " +  markers.get(i).getTitle() + " ',latitude:" + markers.get(i).getLatitude() + ",longitude:" + markers.get(i).getLongitude() + "}";
            if (i != markers.size()-1) {
                str += ",";
            }
        }

        str += "]";


        return str;
    }



    public class Marker {
         String title;
         String info;
        String pathAssetPicture;
    double latitude;
        double longitude;

        Marker() {

        }

        public String getTitle () {
            return title;
        }

        public String getInfo () {
            return info;
        }
        public String getPathAssetPicture () {
            return pathAssetPicture;
        }
        public double getLatitude () {
            return latitude;
        }
        public double getLongitude () {
            return longitude;
        }


        public void Create(double latitude, double longitude) {
            this.title = "Random";
            this.info = "Not provided";
            this.pathAssetPicture = "marker_blue.png";
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public void Create (String title, String info, String pathAssetPicture, double latitude, double longitude) {
            this.title = title;
            this.info = info;
            this.pathAssetPicture = pathAssetPicture;
            this.latitude = latitude;
            this.longitude = longitude;

        }


    }

}
