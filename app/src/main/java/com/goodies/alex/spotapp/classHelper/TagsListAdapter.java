package com.goodies.alex.spotapp.classHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.goodies.alex.spotapp.DataProvider;

import com.goodies.alex.spotapp.R;

import java.util.ArrayList;



/**
 * Created by alex on 2016-12-02.
 */

public class TagsListAdapter extends ArrayAdapter<DataProvider.Marker> {


    public TagsListAdapter(Context context, ArrayList<DataProvider.Marker> markers) {
        super(context, R.layout.marker_cell, markers);

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DataProvider.Marker tag = this.getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.marker_cell, parent, false);
        }

        ImageView markerImage = (ImageView) convertView.findViewById(R.id.markerImage);
        markerImage.setImageResource(R.drawable.marker_blue);

        TextView name = (TextView) convertView.findViewById(R.id.nameTextView);
        name.setText(tag.getTitle());

        TextView latLng = (TextView) convertView.findViewById(R.id.latLngTextView);
        latLng.setText(tag.getLatitude() + "   :    "  + tag.getLongitude());


        return convertView;
    }



    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}
