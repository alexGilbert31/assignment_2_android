package com.goodies.alex.spotapp.fragment;

import android.location.LocationListener;
import com.goodies.alex.spotapp.classHelper.LocationProvider;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodies.alex.spotapp.R;
import com.goodies.alex.spotapp.classHelper.AbstractArchitectCamFragment;
import com.goodies.alex.spotapp.classHelper.WikitudeSDKConstants;
import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;

import java.io.IOException;


public class AugmentedReality extends AbstractArchitectCamFragment {


public ArchitectView architectView;
    private View v;
    private  StartupConfiguration config;
   // privat

    public AugmentedReality() {
        // Required empty public constructor
        Log.d("Constructor", "Caller");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

   /* @Override
    public void onPause() {
        super.onPause();
        try{
            if(architectView!=null) {//

                 getActivity().unregisterReceiver();
            }
        }catch(Exception e)
        {

        }


    }

*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_augmented_reality, container, false);

        architectView = (ArchitectView) v.findViewById( R.id.architectView );
        config = new StartupConfiguration(WikitudeSDKConstants.WIKITUDE_SDK_KEY);
        architectView.onCreate( config );

        //getContext().unregisterReceiver( architectView);

        architectView.setLocation(SpotMapsFragment.mLastLocation.getLatitude(), SpotMapsFragment.mLastLocation.getLongitude(), 100);

        onPostCreate();
        //AbstractArchitectCamFragment.onCreate

       // architectView.onPause(  getActivity().unregisterReceiver(architectView );

        //ArchitectView$NetworkStateReceiver

        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

       /* architectView = (ArchitectView) v.findViewById( R.id.architectView );
        final StartupConfiguration config = new StartupConfiguration(WikitudeSDKConstants.WIKITUDE_SDK_KEY);
        architectView.onCreate( config );

        //getContext().unregisterReceiver( architectView);

        architectView.setLocation(SpotMapsFragment.mLastLocation.getLatitude(), SpotMapsFragment.mLastLocation.getLongitude(), 1); */
        //onPostCreate();
    }

    @Override
    public String getARchitectWorldPath() {
        return "web/index.html";
    }

    @Override
    public ArchitectView.ArchitectUrlListener getUrlListener() {
        return null;
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_augmented_reality;
    }

    @Override
    public String getWikitudeSDKLicenseKey() {
        return WikitudeSDKConstants.WIKITUDE_SDK_KEY;

    }

    @Override
    public int getArchitectViewId() {
        return R.id.architectView;
    }

    @Override
    public ILocationProvider getLocationProvider(LocationListener locationListener) {

        return new LocationProvider(getContext(), locationListener);
    }

    @Override
    public ArchitectView.SensorAccuracyChangeListener getSensorAccuracyListener() {
        return null;
    }


    protected void onPostCreate () {
       architectView.onPostCreate();
        try {
          //  architectView.load("../java/web/index.html");
           // architectView.load("web/index.html");
            architectView.load(getARchitectWorldPath());
//architectView.ar


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
