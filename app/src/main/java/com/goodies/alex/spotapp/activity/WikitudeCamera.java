package com.goodies.alex.spotapp.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.goodies.alex.spotapp.R;
import com.goodies.alex.spotapp.classHelper.WikitudeSDKConstants;
import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;

import java.io.IOException;

public class WikitudeCamera extends AppCompatActivity {


    private ArchitectView architectView;

    private static final int WIKITUDE_PERMISSIONS_REQUEST_CAMERA = 0;
    private static final int WIKITUDE_PERMISSIONS_REQUEST_GPS = 0;
    private static final int WIKITUDE_PERMISSIONS_NETWORK_STATE = 0;
    private static final int WIKITUDE_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;
    private static final int WIKITUDE_PERMISSIONS_INTERNET = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wikitude_camera);

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, WIKITUDE_PERMISSIONS_REQUEST_GPS);
        }

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, WIKITUDE_PERMISSIONS_NETWORK_STATE);
        }

        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, WIKITUDE_PERMISSIONS_REQUEST_CAMERA);
        }

        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WIKITUDE_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
        }
        if ( ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, WIKITUDE_PERMISSIONS_INTERNET);
        }
       // CameraControls camCtlr = new CameraControls(400,400);
       // camCtlr.getImageSensorRotation();
        //test

        this.architectView = (ArchitectView)this.findViewById( R.id.architectViewNew );
        final StartupConfiguration config = new StartupConfiguration(WikitudeSDKConstants.WIKITUDE_SDK_KEY);
        this.architectView.onCreate( config );


    }


    @Override
    public void onPostCreate (Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.architectView.onPostCreate();
        try {
            this.architectView.load("../web/index.html");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

}

