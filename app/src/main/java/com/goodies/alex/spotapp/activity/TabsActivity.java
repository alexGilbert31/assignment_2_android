package com.goodies.alex.spotapp.activity;


import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.goodies.alex.spotapp.R;
import com.goodies.alex.spotapp.fragment.ARLocationImproved;
import com.goodies.alex.spotapp.fragment.PageFragment;
import com.goodies.alex.spotapp.fragment.SpotMapsFragment;
import com.goodies.alex.spotapp.fragment.TagsFragment;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.fragment;


public class TabsActivity extends AppCompatActivity {

        private Toolbar toolbar;
        private TabLayout tabLayout;
        private ViewPager viewPager;


    PageFragment page;
    SpotMapsFragment maps;
    ARLocationImproved ar;
    TagsFragment tag;

    private int numOfTabs = 4;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();


        //Allow to stay render all the screen
        viewPager.setOffscreenPageLimit(numOfTabs + 1);

      //TODO:
        //  getSupportFragmentManager().beginTransaction().add(R.id.tagsListFragment, tag,"tagsListFragment").commit();

        // ArchitectView.NetworkStateReceiver.PendingResult;

      //  ArchitectView.NetworkStateReceiver(this)


       // unregisterReceiver(new );



    }

    private void setupTabIcons() {
        //tabIcons.length must equal numOfTabs;
        int[] tabIcons = {
                R.drawable.ic_tab_ticket,
                R.drawable.ic_tab_map,
                R.drawable.ic_tab_camera,
          //      R.drawable.ic_tab_camera,
                R.drawable.ic_tab_places

        };
        for (int i=0;i<numOfTabs;i++) {
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
    }




    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

         page = new PageFragment();
         maps = new SpotMapsFragment();
         ar = new ARLocationImproved();
         tag = new TagsFragment();

        adapter.addFrag(page, "SpotPage");
        adapter.addFrag(maps, "SpotMaps");
        adapter.addFrag(ar, "SpotCameraImproved");
        // adapter.addFrag(new AugmentedReality(), "SpotCamera");
        adapter.addFrag(tag, "SpotTags");


        viewPager.setAdapter(adapter);


    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }
}
