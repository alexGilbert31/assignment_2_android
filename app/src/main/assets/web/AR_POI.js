// implementation of AR-Experience (aka "World")
var World = {
	// true once data was fetched
	initiallyLoadedData: false,
	stopIt:false,

	// POI-Marker asset
	markerDrawable_idle: null,
	latUser: null,
	lonUser:null,
	altUser: null,

	// called to inject new POI data
	loadPoisFromJsonData: function loadPoisFromJsonDataFn(poiData) {

	var markers = JSON.parse(poiData);
    	console.log(markers);
    	alert("createmarker call");

    	for(var i=0;i<markers.length;i++) {

    	var tag = {
                    "id": i,
                    "latitude": markers[i].latitude,
                    "longitude": markers[i].longitude,
                    "altitude": 100.0,
                    "color": "blue"
                };



		/*
			The example Image Recognition already explained how images are loaded and displayed in the augmented reality view. This sample loads an AR.ImageResource when the World variable was defined. It will be reused for each marker that we will create afterwards.
		*/
		World.markerDrawable_idle = new AR.ImageResource("img/marker_" +tag.color + ".png" );


		/*
			For creating the marker a new object AR.GeoObject will be created at the specified geolocation. An AR.GeoObject connects one or more AR.GeoLocations with multiple AR.Drawables. The AR.Drawables can be defined for multiple targets. A target can be the camera, the radar or a direction indicator. Both the radar and direction indicators will be covered in more detail in later examples.
		*/
		var markerLocation = new AR.GeoLocation(tag.latitude, tag.longitude, tag.altitude);


		//var userLocation = new AR.GeoLocation(latUser, lonUser, altUser);

		//var close = new AR.RelativeLocation();



		var markerImageDrawable_idle = new AR.ImageDrawable(World.markerDrawable_idle, 2.5, {
			zOrder: 0,
			opacity: 1.0
		});



		// create GeoObject
		var markerObject = new AR.GeoObject(markerLocation, {
			drawables: {
				cam: [markerImageDrawable_idle]
			}


		});




		// Updates status message as a user feedback that everything was loaded properly.
		World.updateStatusMessage('1 place loaded');

		}

	},

	// updates status message shon in small "i"-button aligned bottom center
	updateStatusMessage: function updateStatusMessageFn(message, isWarning) {

		var themeToUse = isWarning ? "e" : "c";
		var iconToUse = isWarning ? "alert" : "info";

		$("#status-message").html(message);
		$("#popupInfoButton").buttonMarkup({
			theme: themeToUse
		});
		$("#popupInfoButton").buttonMarkup({
			icon: iconToUse
		});
	},


	// location updates, fired every time you call architectView.setLocation() in native environment
	locationChanged: function locationChangedFn(lat, lon, alt, acc) {

	latUser = lat;
	lonUser= lon;
	altUser = alt;
	//alert("call");

	    //alert(acc);

		/*

			The custom function World.onLocationChanged checks with the flag World.initiallyLoadedData if the function was already called. With the first call of World.onLocationChanged an object that contains geo information will be created which will be later used to create a marker using the World.loadPoisFromJsonData function.
		*/
		//alert("poiData call before if");
		if (!this.stopIt) {
            alert("poiData call begin");


			// creates a poi object with a random location near the user's location
			//VFS close location: 49.280763, -123.102083
		/*	var poiData = {
				"id": 0,
				"latitude": 49.2803583,
				"longitude": -123.1030802,
				"altitude": 1.0,
				"color": "blue"
			};

                            var poiData2 = {
                                            "id": 1,
                                            "latitude": 49.280273,
                                            "longitude": -123.101812,
                                            "altitude": 1.0,
                                            "color": "orange"
                                        };
                                        var poiData3 = {
                                                                    "id": 2,
                                                                    "latitude": 49.280073,
                                                                    "longitude": -123.101412,
                                                                    "altitude": 1.0,
                                                                    "color": "blue"
                                                                };
                                                                var poiData4 = {
                                                                                            "id": 3,
                                                                                            "latitude": 49.280413,
                                                                                            "longitude": -123.101212,
                                                                                            "altitude": 1.0,
                                                                                            "color": "orange"
                                                                                        };
            World.initiallyLoadedData = true;
            this.stopIt = true;

			World.loadPoisFromJsonData(poiData);
			World.loadPoisFromJsonData(poiData2);
			World.loadPoisFromJsonData(poiData3);
			World.loadPoisFromJsonData(poiData4);
			//World.initiallyLoadedData = true;
*/


			alert("poiData call end");
			this.stopIt = true;
			//World.initiallyLoadedData = true;
		}
	},
};

/*
	Set a custom function where location changes are forwarded to. There is also a possibility to set AR.context.onLocationChanged to null. In this case the function will not be called anymore and no further location updates will be received.
*/
AR.context.onLocationChanged = World.locationChanged;
//AR.context.onCreateMarker = World.createMarker;


